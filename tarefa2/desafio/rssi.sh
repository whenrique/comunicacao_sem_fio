#!/bin/bash
	SIZE=10000 
	COUNTER=0
	echo "rssi = [ " > rssi_matlab.m
        while [  $COUNTER -lt $SIZE ]; do
		var=$(iwconfig wlan0 | grep level )
 	        echo ${var:43:3} " " >> rssi_matlab.m
		let COUNTER=COUNTER+1 
        	sleep 0.01
	done
	
	echo " ];" >> rssi_matlab.m         
	tr -d '\n' < rssi_matlab.m > rssi_matlab_new.m
