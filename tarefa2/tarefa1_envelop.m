close all
clear all
clc

h(1) = 0;
K = 10;
Pr = 1;
m = 1;
for i=1:500
  ph_ray(i) =  (2*h(i)/Pr)*exp(-h(i)^2/Pr);
  ph_rice(i) = (2*h(i)*(K + 1)/Pr)*exp(-K - ((K+1)*h(i)^2)/Pr) * besseli(0, ( 2*h(i)*sqrt(K*(K+1)/Pr) ) );
  ph_naka(i) = (((2*m^m)*h(i)^(2*m-1))/(gamma(m)*Pr^m))*exp(-(m*(h(i)^2))/(Pr));
  h(i+1) = h(i) + 0.01;
end
h = h(1:end-1);
plot(h, ph_ray);
hold
plot(h, ph_rice, 'r');

figure 

plot(h, ph_naka);