clear all
close all
clc

gamma_medio = 100; %20dB

Pout = 0.0001:0.0001:0.1;
Co = (1 - Pout )*log2(1 + gamma_medio );

%x = r(1:20);
loglog( Pout, Co);
xlabel('Probabilidade de outage');
ylabel('C/B');
xlim(e-4, e-1);
grid
#{
for i = 1:9
    r = 0:0.5:4;
    gamma_min = (2^r)-1
    C_B(i) = log2(1+((2^r(i))-1))
    P_out_ray(i) = 1 - exp(-((2^r(i))-1)/gamma_medio)
    P_out_ray(i) = (2^r(i) - 1)/gamma_medio
    
end
#}