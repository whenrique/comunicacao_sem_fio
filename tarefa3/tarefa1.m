clc
clear all
close all

Pr_d0 = 0.01/((4*pi)^2)*10
No = 10^(-204/10);
B = 100e6;
r = 1;
n = 4;
m_2 = 2;
m_6 = 3;

for i=1:500
  
  d(i) = i;
  Pr(d(i)) = Pr_d0*((1/d(i))^n);
  %Pout(d) = 1 - e^(-1/(Pr(d)/No*B));
  %gama(i) = 10^(d(i)/10);
  
  gama(i) = Pr(d(i))/(No*B);
  pout_rayleigh(i) = 1 - e^(-(2^r-1)/gama(i));
  pout_nakagami_2(i) = gammainc( ((m_2*(2^r - 1))/gama(i)),m_2)/gamma(m_2);
end

loglog(d, pout_rayleigh);
hold
loglog(d, pout_nakagami_2, 'r');
grid;
title("funçao loglog");
legend('rayleigh', 'nakagami-2');
xlabel("Distancia");
ylabel("Probabilidade outage");
figure
plot(d, pout_rayleigh);
hold
plot(d, pout_nakagami_2, 'r');
grid;
xlabel("Distancia");
ylabel("Probabilidade outage");
legend('rayleigh', 'nakagami-2');
