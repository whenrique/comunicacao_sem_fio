clear all
close all

data = randi([0 1],1000,1);
%trellis = poly2trellis([5 4],[23 35 0; 0 5 13]);
%trellis = poly2trellis(7,[171 133]);
trellis = poly2trellis(3,[7 5]);

codedData = convenc(data,trellis);

decodedData = vitdec(codedData,trellis,34,'trunc','hard');

%code1 = convenc(data,poly2trellis([5 4],[23 35 0; 0 5 13]));

