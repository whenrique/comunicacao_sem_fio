close all;
clear all;

rand('state',0); %Reinicializa sementes de geradores de n�meros aleat�rios
randn('state',0);

bits=1e6; %N�mero de bits transmitidos

M=2; %BPSK, dois s�mbolos poss�veis na modula��o.
trellis_7_5 = poly2trellis(3,[7 5]);
trellis_171_133 = poly2trellis(7,[171 133]);
b=rand(1,bits)>0.5; %Gera 0s e 1s
codedData_7_5 = convenc(b,trellis_7_5);
codedData_171_133 = convenc(b,trellis_171_133);

x=2*b-1; %Gera s�mbolos com energia unit�ria
x_coded_7_5 = 2*codedData_7_5 -1;
x_coded_171_133 = 2*codedData_171_133 -1;
N0=1; %Neste exemplo N0 � fixa em 1
n_FEC=randn(1,bits*2)*sqrt(N0/2); %Ru�do base com variancia N0/2
n=randn(1,bits)*sqrt(N0/2); %Ru�do base com variancia N0/2
h_rayleigh_dist = makedist('rayleigh');
h_rayleigh_FEC = random(h_rayleigh_dist, bits*2,1)*sqrt(0.5);
h_rayleigh = random(h_rayleigh_dist, bits,1)*sqrt(0.5);


for i=0:30
   
    EbN0dB=i; %Valor de Eb/N0 em dB a ser considerado
    EbN0_medio=10^(EbN0dB/10); %Valor de Eb/N0 em linear
    EbN0 = EbN0_medio*(h_rayleigh_FEC.^2);
    Eb=EbN0*N0; %C�lculo de Eb
    Es=Eb/log2(M); %C�lculo de Es
    y=sqrt(Es)'.*x_coded_7_5 +n_FEC; %Sinal recebido (sa�da do canal Rayleigh)
    b_est=y>0; %Decisor no receptor
    decodedData = vitdec(b_est,trellis_7_5,34,'trunc','hard');
    erros=sum(b~=decodedData); %Contagem de erros de bit
    ber_rayleigh_FEC_7_5(i + 1) = erros/bits; %C�lculo da BER simulada
    xaxis(i+1) = i;    
    
    EbN0dB=i; %Valor de Eb/N0 em dB a ser considerado
    EbN0_medio=10^(EbN0dB/10); %Valor de Eb/N0 em linear
    EbN0 = EbN0_medio*(h_rayleigh_FEC.^2);
    Eb=EbN0*N0; %C�lculo de Eb
    Es=Eb/log2(M); %C�lculo de Es
    y=sqrt(Es)'.*x_coded_171_133 +n_FEC; %Sinal recebido (sa�da do canal Rayleigh)
    b_est=y>0; %Decisor no receptor
    decodedData = vitdec(b_est,trellis_171_133,34,'trunc','hard');
    erros=sum(b~=decodedData); %Contagem de erros de bit
    ber_rayleigh_FEC_171_133(i + 1) = erros/bits; %C�lculo da BER simulada
    xaxis(i+1) = i;    
    
    
    EbN0dB=i; %Valor de Eb/N0 em dB a ser considerado
    EbN0_medio=10^(EbN0dB/10); %Valor de Eb/N0 em linear
    EbN0 = EbN0_medio*(h_rayleigh.^2);
    Eb=EbN0*N0; %C�lculo de Eb
    Es=Eb/log2(M); %C�lculo de Es
    y=sqrt(Es)'.*x +n; %Sinal recebido (sa�da do canal Rayleigh)
    b_est=y>0; %Decisor no receptor
    erros=sum(b~=b_est); %Contagem de erros de bit
    ber_rayleigh(i + 1) = erros/bits; %C�lculo da BER simulada
    
end
xaxis = (0:30);
semilogy(xaxis, ber_rayleigh);
hold
semilogy(xaxis, ber_rayleigh_FEC_7_5 );
semilogy(xaxis, ber_rayleigh_FEC_171_133);
axis([0 30 1e-5 1])
grid
legend('Ray', 'Ray_7_5', 'Ray_{171}-_{133}');