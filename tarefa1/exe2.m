close all
clear all
clc

fc = 1800;
hte = 40;
hre = 3;
cm = 3;
d = [000:1000:20000];

Pt = 37.559; % in db

Pr_1000 = 1e-6;
Pr_1000_db = 10*log10(Pr_1000);
Pr_1000_dbm = Pr_1000_db + 30;

ahre = (1.1*log10(fc) - 0.7)*hre - (1.56*log10(fc) - 0.8)  ;% 3.2*((log10(11.75*hre))^2) - 4.97;

for i = 1:20
  x(i) = i - 1;
  Pr_dBm_EL(i) = Pr_1000_dbm + 20*log10(1000/d(i));
  Pr_dBm_LD_3(i) = Pr_1000_dbm + 30*log10(1000/d(i));
  Pr_dBm_LD_4(i) = Pr_1000_dbm + 40*log10(1000/d(i));
  Pr_hata(i) = 46.3 + 33.9*log10(fc) - 13.82*log10(hte) - ahre + (44.9 - 6.55*log10(hte))*log10(d(i)/1000) + cm;
  Pr_hata(i) = 103 - Pr_hata(i) ;
end


loglog(x, Pr_dBm_EL, 'r')
hold
loglog(x, Pr_dBm_LD_3, 'g')
loglog(x, Pr_dBm_LD_4, 'y')
loglog(x, Pr_hata, 'b')
grid
xlim([0 20]);
ylim([-100 0]);
xlabel('distância');
ylabel('Potência em dBm');
legend('Espaço Livre', 'Log-Distancia3', 'Log-Distancia4', 'hata');