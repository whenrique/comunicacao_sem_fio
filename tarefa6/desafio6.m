close all;
clear all;

rand('state',0); %Reinicializa sementes de geradores de n�meros aleat�rios
randn('state',0);

bits=1e6; %N�mero de bits transmitidos

M=2; %BPSK, dois s�mbolos poss�veis na modula��o.
b=rand(1,bits)>0.5; %Gera 0s e 1s


x=2*b-1; %Gera s�mbolos com energia unit�ria
N0=1; %Neste exemplo N0 � fixa em 1
n=randn(1,bits)*sqrt(N0/2); %Ru�do base com variancia N0/2
n_2=randn(1,bits)*sqrt(N0/2); %Ru�do base com variancia N0/2
h_rayleigh_dist = makedist('rayleigh');
h_rayleigh = random(h_rayleigh_dist, bits,1)*sqrt(0.5);

h_rayleigh_2 = random(h_rayleigh_dist, bits,1)*sqrt(0.5);

h_rayleigh_3 = random(h_rayleigh_dist, bits,1)*sqrt(0.5);

h_rayleigh_4 = random(h_rayleigh_dist, bits,1)*sqrt(0.5);

for i=0:20
       
    EbN0dB=i; %Valor de Eb/N0 em dB a ser considerado
    EbN0_medio=10^(EbN0dB/10); %Valor de Eb/N0 em linear
    EbN0 = EbN0_medio*(h_rayleigh.^2) ;
    Eb=EbN0*N0; %C�lculo de Eb
    Es=Eb/log2(M); %C�lculo de Es
    y=sqrt(Es)'.*x +n; %Sinal recebido (sa�da do canal Rayleigh)
    b_est=y>0; %Decisor no receptor
    erros=sum(b~=b_est); %Contagem de erros de bit
    ber_rayleigh(i + 1) = erros/bits; %C�lculo da BER simulada
    
    EbN0dB=i; %Valor de Eb/N0 em dB a ser considerado
    EbN0_medio=10^(EbN0dB/10); %Valor de Eb/N0 em linear
    for j=1:bits
        if(h_rayleigh(j) > h_rayleigh_2(j))
            EbN0(j) = EbN0_medio*(h_rayleigh(j)^2);
        else
            EbN0(j) = EbN0_medio*(h_rayleigh_2(j)^2);
        end
    end
    Eb=EbN0*N0; %C�lculo de Eb
    Es=Eb/log2(M); %C�lculo de Es
    y=(sqrt(Es)'.*x +n); %Sinal recebido (sa�da do canal Rayleigh)
    b_est=y>0; %Decisor no receptor
    erros_SC=sum(b~=b_est); %Contagem de erros de bit
    ber_rayleigh_SC(i + 1) = erros_SC/bits; %C�lculo da BER simulada
    
    EbN0dB=i; %Valor de Eb/N0 em dB a ser considerado
    EbN0_medio=10^(EbN0dB/10); %Valor de Eb/N0 em linear
    EbN0 = EbN0_medio*(h_rayleigh.^2) + EbN0_medio*(h_rayleigh_2.^2);
    Eb=EbN0*N0; %C�lculo de Eb
    Es=Eb/log2(M); %C�lculo de Es
    y=(sqrt(Es)'.*x +n); %Sinal recebido (sa�da do canal Rayleigh)
    b_est=y>0; %Decisor no receptor
    erros_MRC=sum(b~=b_est); %Contagem de erros de bit
    ber_rayleigh_MRC(i + 1) = erros_MRC/bits; %C�lculo da BER simulada
    
    EbN0dB=i; %Valor de Eb/N0 em dB a ser considerado
    EbN0_medio=10^(EbN0dB/10); %Valor de Eb/N0 em linear
    EbN0 = EbN0_medio*(h_rayleigh.^2) + EbN0_medio*(h_rayleigh_2.^2) +  EbN0_medio*(h_rayleigh_3.^2)+  EbN0_medio*(h_rayleigh_4.^2);
    Eb=EbN0*N0; %C�lculo de Eb
    Es=Eb/log2(M); %C�lculo de Es
    y=(sqrt(Es)'.*x +n); %Sinal recebido (sa�da do canal Rayleigh)
    b_est=y>0; %Decisor no receptor
    erros_MRC_4=sum(b~=b_est); %Contagem de erros de bit
    ber_rayleigh_MRC_4(i + 1) = erros_MRC_4/bits; %C�lculo da BER simulada
  
    EbN0dB=i; %Valor de Eb/N0 em dB a ser considerado
    EbN0_medio=10^(EbN0dB/10); %Valor de Eb/N0 em linear
    for j=1:bits
        if(h_rayleigh(j) > h_rayleigh_2(j) &&  h_rayleigh(j) > h_rayleigh_3(j)  &&  h_rayleigh(j) > h_rayleigh_4(j))
            EbN0(j) = EbN0_medio*(h_rayleigh(j)^2);
        elseif(h_rayleigh_2(j) > h_rayleigh(j) &&  h_rayleigh_2(j) > h_rayleigh_3(j)  &&  h_rayleigh_2(j) > h_rayleigh_4(j))
            EbN0(j) = EbN0_medio*(h_rayleigh_2(j)^2);
        elseif(h_rayleigh_3(j) > h_rayleigh(j) &&  h_rayleigh_3(j) > h_rayleigh_2(j)  &&  h_rayleigh_3(j) > h_rayleigh_4(j))
            EbN0(j) = EbN0_medio*(h_rayleigh_3(j)^2);
        else 
            EbN0(j) = EbN0_medio*(h_rayleigh_4(j)^2);
        end
    end
    Eb=EbN0*N0; %C�lculo de Eb
    Es=Eb/log2(M); %C�lculo de Es
    y=(sqrt(Es)'.*x +n); %Sinal recebido (sa�da do canal Rayleigh)
    b_est=y>0; %Decisor no receptor
    erros_SC_4=sum(b~=b_est); %Contagem de erros de bit
    ber_rayleigh_SC_4(i + 1) = erros_SC_4/bits; %C�lculo da BER simulada
end
xaxis = (0:20);
semilogy(xaxis, ber_rayleigh);
hold
semilogy(xaxis, ber_rayleigh_MRC);
semilogy(xaxis, ber_rayleigh_SC, '*');
semilogy(xaxis, ber_rayleigh_MRC_4);
semilogy(xaxis, ber_rayleigh_SC_4, '*');
axis([0 20 1e-4 1])
grid
legend('M=1', 'MRC M=2', 'SC M=2', 'MRC M=4', 'SC M=4');